use clap::{Parser, Subcommand};

#[derive(Parser, Debug)]
#[command(author, version, about)]
pub struct Cli {
    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand, Debug)]
pub enum Commands {
    /// Starts the time tracking for a given project.
    Start {
        /// Which project should be tracked.
        project: String,

        /// Which task is associated with the tracking.
        task: Option<String>,
    },
    /// Stops the time tracking for the current project.
    Stop {},
    /// Generates a terminal report.
    Report {
        #[command(subcommand)]
        frame: Option<TimeFrame>,
    },
}

#[derive(Subcommand, Debug)]
pub enum TimeFrame {
    /// Report a day.
    Day {
        /// Optional specific date, defaults to `today`.
        date: Option<String>,
    },
    /// Report a week.
    Week {
        /// Optional specific week number, defaults to the current week.
        number: Option<u32>,
    },
    /// Report a month.
    Month {
        /// Optional specific month number, defaults to the current month.
        number: Option<u32>,
    },
}
