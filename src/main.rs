use std::{
    fs::{self, OpenOptions},
    io::Write,
};

use anyhow::{Context, Result};
use chrono::{DateTime, Datelike, Utc};

use std::collections::HashMap;

mod cli;
use clap::Parser;
use cli::Cli;
use serde::{Deserialize, Serialize};
use xdg::BaseDirectories;

use owo_colors::OwoColorize;

fn main() -> Result<()> {
    let args = Cli::parse();

    match args.command {
        cli::Commands::Start { project, task } => {
            handle_start(&project, task).context(format!("Tried to start project {}", project))?
        }
        cli::Commands::Stop {} => handle_stop()?,
        cli::Commands::Report { frame } => handle_report(frame)?,
    }

    Ok(())
}

fn data_file() -> Result<String> {
    let base_dirs = BaseDirectories::new().context("Failed to initialize XDG base dir")?;
    let mut data_dir = base_dirs.get_data_home();
    data_dir.push("ttracker");
    fs::create_dir_all(&data_dir).context("Could not create data directory!")?;

    let current_date = Utc::now();
    data_dir.push(format!(
        "{}-{:02}",
        current_date.year(),
        current_date.month()
    ));
    data_dir.set_extension("json");
    Ok(data_dir.to_str().unwrap().to_owned())
}

fn handle_report(_frame: Option<cli::TimeFrame>) -> Result<()> {
    let path = data_file()?;
    let data = read_data(&path);

    let mut map: HashMap<String, chrono::Duration> = HashMap::new();

    data.iter().for_each(|d| {
        let end = d.end.unwrap_or(Utc::now());
        let delta = end - d.start;
        let e = map.entry(d.project.to_owned());
        e.and_modify(|dur| *dur = *dur + delta).or_insert(delta);
    });

    let mut key_map: Vec<_> = map.iter().collect();
    key_map.sort_by_key(|&(_, v)| v);
    key_map.reverse();

    key_map.iter().for_each(|(k, v)| {
        println!(
            "{:>20}: {:02}:{:02}",
            k.bold().cyan(),
            v.num_hours(),
            v.num_minutes() % 60
        );
    });

    Ok(())
}

#[derive(Serialize, Deserialize, Debug)]
struct TimeSlot {
    start: DateTime<Utc>,
    end: Option<DateTime<Utc>>,
    project: String,
    task: Option<String>,
}

fn read_data(path: &str) -> Vec<TimeSlot> {
    let file = OpenOptions::new().read(true).open(path);
    match file {
        Ok(f) => serde_json::from_reader(f).unwrap_or_default(),
        _ => vec![],
    }
}

fn write_data(path: &str, data: &[TimeSlot]) -> Result<()> {
    let mut file = OpenOptions::new()
        .write(true)
        .truncate(true)
        .create(true)
        .open(path)?;

    write!(file, "{}", serde_json::json!(data))?;
    Ok(())
}

fn handle_start(project: &str, task: Option<String>) -> Result<()> {
    let path = data_file()?;
    let mut v = read_data(&path);

    let now = Utc::now();
    v.iter_mut().for_each(|ts| {
        if ts.end.is_none() {
            ts.end = Some(now)
        }
    });

    v.push(TimeSlot {
        start: now,
        end: None,
        project: project.to_owned(),
        task,
    });

    write_data(&path, &v)?;
    Ok(())
}

fn handle_stop() -> Result<()> {
    let path = data_file()?;
    let mut v = read_data(&path);

    let now = Utc::now();
    v.iter_mut().for_each(|ts| {
        if ts.end.is_none() {
            ts.end = Some(now)
        }
    });

    write_data(&path, &v)?;
    Ok(())
}
